import { Route, Switch } from "react-router";
import { Routes } from "./utils/routes";
import { useCheckingUserAuthorization } from "./hooks/use-local-storage/use-checking-user-authorization.ts";
import MainPage from "./components/pages/main-page/main-page";
import LoginPage from "./components/pages/login-page/login-page";
import "./App.scss";

function App() {
  useCheckingUserAuthorization();

  return (
    <div className="App">
      <Switch>
        <Route path={Routes.mainPage} component={MainPage} />
        <Route path={Routes.loginPage} component={LoginPage} />
      </Switch>
    </div>
  );
}

export default App;
