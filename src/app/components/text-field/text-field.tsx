import { useState } from "react";
import { TTextField } from "./types/type-text-field";

const TextField = ({
  label,
  type,
  name,
  value,
  onChange,
  placeholder,
  errorClassName,
  error,
  className,
}: TTextField) => {
  const [isShowError, setIsShowError] = useState(false);
  const handleClick = () => {
    if (!isShowError) {
      setIsShowError(true);
    }
  };
  return (
    <>
      <div>
        <label htmlFor={label}>{label}</label>
      </div>
      {error && isShowError && (
        <div className={errorClassName ? errorClassName : "error"}>{error}</div>
      )}
      <input
        placeholder={placeholder}
        type={type}
        name={name}
        value={value}
        onChange={onChange}
        className={className}
        onFocus={handleClick}
      />
    </>
  );
};

export default TextField;
