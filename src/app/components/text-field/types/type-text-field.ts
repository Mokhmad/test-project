export type TTextField = {
  label?: string;
  type: string;
  name?: string;
  value: string;
  placeholder: string;
  onChange: (e: React.FocusEvent<HTMLInputElement>) => void;
  error?: string;
  className?: string;
  errorClassName?: string;
};
