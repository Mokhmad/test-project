import "../header/header.scss";
import { THeader } from "./types/types-header";

const Header = ({ linksName }: THeader) => {
  return <div className="header">{linksName}</div>;
};

export default Header;
