import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { IoIosArrowForward } from "react-icons/io";
import "./breadcrumbs.scss";

const Breadcrumbs = () => {
  const history = useHistory();
  const {
    location: { pathname },
  } = history;

  const [pathnames, setPathnames] = useState<string[]>();

  useEffect(() => {
    setPathnames(pathname.split("/").filter((x) => x));
  }, [pathname]);

  return (
    <div className="breadcrumbs">
      {pathnames &&
        pathnames.map((pathname, index) => {
          const routeTo = `/${pathnames.slice(0, index + 1).join("/")}`;
          const isLast = index === pathnames.length - 1;
          return isLast ? (
            <div key={pathname} className="lastBreadcrumb">
              {pathname}
            </div>
          ) : (
            <div key={pathname} className="breadcrumb">
              <div
                onClick={() => history.push(routeTo)}
                className="breadcrumbName"
              >
                {pathname}
              </div>
              <div className="breadcrumbArrow">
                <IoIosArrowForward />
              </div>
            </div>
          );
        })}
    </div>
  );
};

export default Breadcrumbs;
