import { TCommentary } from "../types/comment-type";
import { AiFillEdit } from "react-icons/ai";
import { useModal } from "../../../hooks/use-modal-context/use-modal-context";
import { TComment } from "../../../redux/types/types-comments";
import Button from "../../button/button";
import "./commentary.scss";

const Commentary = ({ commentary }: TCommentary) => {
  const { getCommentData, toggleModalEdit } = useModal();
  const handleClick = (data: TComment) => {
    getCommentData(data);
    toggleModalEdit();
  };
  return (
    <div className="commentary">
      <div className="commentName">
        <p>{commentary.name}</p>
      </div>
      <div className="commentBody">
        <p>{commentary.body}</p>
      </div>
      <div className="commentEmail">
        <p>{commentary.email}</p>
      </div>
      <Button
        type="button"
        className="editCommentBtn"
        icon={<AiFillEdit />}
        onClick={() => handleClick(commentary)}
      />
    </div>
  );
};

export default Commentary;
