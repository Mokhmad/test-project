import { useState } from "react";
import { useDispatch } from "react-redux";
import {
  sortBodyComments,
  sortEmailComments,
  sortNameComments,
} from "../../../redux/actions/comments-actions";
import { TiArrowSortedDown, TiArrowSortedUp } from "react-icons/ti";
import Button from "../../button/button";
import "./table-comments-sub-header.scss";

const TableCommentsSubHeader = () => {
  const dispatch = useDispatch();
  const [sortName, setSortName] = useState(false);
  const [sortBody, setSortBody] = useState(false);
  const [sortEmail, setSortEmail] = useState(false);

  const handleSortName = () => {
    dispatch(sortNameComments(sortName));
    setSortName(!sortName);
  };

  const handleSortBody = () => {
    dispatch(sortBodyComments(sortBody));
    setSortBody(!sortBody);
  };

  const handleSortEmail = () => {
    dispatch(sortEmailComments(sortEmail));
    setSortEmail(!sortEmail);
  };

  return (
    <div className="commentsHeader">
      <div>
        Name
        <Button
          type="button"
          text="sort"
          onClick={handleSortName}
          className="btnSort"
        />
        <div>{sortName ? <TiArrowSortedDown /> : <TiArrowSortedUp />}</div>
      </div>
      <div>
        Body
        <Button
          type="button"
          text="sort"
          onClick={handleSortBody}
          className="btnSort"
        />
        <div>{sortBody ? <TiArrowSortedDown /> : <TiArrowSortedUp />}</div>
      </div>
      <div>
        Email
        <Button
          type="button"
          text="sort"
          onClick={handleSortEmail}
          className="btnSort"
        />
        <div>{sortEmail ? <TiArrowSortedDown /> : <TiArrowSortedUp />}</div>
      </div>
    </div>
  );
};

export default TableCommentsSubHeader;
