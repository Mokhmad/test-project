import { TComment } from "../../../redux/types/types-comments";

export type TCommentary = {
  key: number;
  commentary: TComment;
};

export type TCommnetHeader = {
  filterChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  searchComment: string;
};
