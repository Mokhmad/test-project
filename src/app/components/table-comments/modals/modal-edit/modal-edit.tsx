import { MdOutlineCancel } from "react-icons/md";
import { useDispatch } from "react-redux";
import { toggleModalEditComment } from "../../../../redux/actions/comments-actions";
import { useModal } from "../../../../hooks/use-modal-context/use-modal-context";
import Button from "../../../button/button";
import "./modal-edit-comment.scss";

const ModalEdit = () => {
  const dispatch = useDispatch();
  const handleEdit = () => {
    toggleModalEdit();
    dispatch(toggleModalEditComment());
  };
  const handleModalDelete = () => {
    toggleModalEdit();
    toggleModalDelete();
  };

  const { toggleModalEdit, toggleModalDelete } = useModal();

  return (
    <div className="modalEditComment">
      <div className="modalEditCommentBody">
        <div>
          <Button
            type="button"
            className="editComBtn"
            text="Редактировать"
            onClick={handleEdit}
          />
        </div>
        <div>
          <Button
            type="button"
            className="deleteComBtn"
            text="Удалить"
            onClick={handleModalDelete}
          />
        </div>
        <Button
          type="button"
          icon={<MdOutlineCancel />}
          className="btnEditModalCancel"
          onClick={toggleModalEdit}
        />
      </div>
    </div>
  );
};

export default ModalEdit;
