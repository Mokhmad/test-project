import React, { useEffect, useState } from "react";
import { MdOutlineCancel } from "react-icons/md";
import { useDispatch } from "react-redux";
import { useModal } from "../../../../hooks/use-modal-context/use-modal-context";
import {
  editComment,
  toggleModalEditComment,
} from "../../../../redux/actions/comments-actions";
import { validator } from "../../../../utils/validator";
import Button from "../../../button/button";
import TextField from "../../../text-field/text-field";
import { TCommentData, TErrorCommentData } from "../types/modal-types";
import "./modal-edit-comment.scss";

const ModalEditComment = () => {
  const dispatch = useDispatch();
  const { commentData } = useModal();
  const [data, setData] = useState<TCommentData>({
    id: commentData.id,
    name: commentData.name,
    email: commentData.email,
    body: commentData.body,
    postId: commentData.postId,
  });
  const [errors, setErrors] = useState<TErrorCommentData>({
    name: "",
    email: "",
    body: "",
  });

  const isChanging = JSON.stringify(data) === JSON.stringify(commentData);

  const handleChange = (e: React.SyntheticEvent): void => {
    const target = e.target as HTMLInputElement;
    setData((prevState) => ({
      ...prevState,
      [target.name]: target.value,
    }));
  };

  useEffect(() => {
    validate();
  }, [data]);

  const validatorConfig = {
    name: {
      isRequired: {
        message: "Имя обезательно для заполнения",
      },
    },
    email: {
      isRequired: {
        message: "Электронная почта обязательна для заполнения",
      },
      isEmail: {
        message: "Email введен некорректно",
      },
    },
    body: {
      isRequired: {
        message: "Текст комментария обязательна для заполнения",
      },
    },
  };

  const validate = () => {
    const errors = validator(data, validatorConfig);
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const isValid = Object.keys(errors).length === 0;

  const handleSubmit = () => {
    dispatch(editComment(data));
  };

  const handleModalEditComment = () => {
    dispatch(toggleModalEditComment());
  };

  return (
    <div className="editModalComment">
      <div className="editModalCommentBody">
        <h2>Edit Comment</h2>
        <TextField
          name="name"
          type="text"
          placeholder="Введите имя"
          value={data.name}
          onChange={handleChange}
          error={errors.name}
          className="editCommentInput"
          errorClassName="editModalNameError"
        />
        <TextField
          name="email"
          type="email"
          placeholder="Введите почту"
          value={data.email}
          onChange={handleChange}
          error={errors.email}
          className="editCommentInput"
          errorClassName="editModalEmailError"
        />
        <TextField
          name="body"
          type="text"
          placeholder="Введите текст"
          value={data.body}
          onChange={handleChange}
          error={errors.body}
          className="editCommentInput"
          errorClassName="editModalBodyError"
        />
        <div>
          <Button
            disabled={isChanging || !isValid}
            type="button"
            text="Сохранить"
            className="editCommentBtnModal"
            onClick={handleSubmit}
          />
          <Button
            type="button"
            text="отмена"
            className="cancelBtnEditModal"
            onClick={handleModalEditComment}
          />
        </div>
        <Button
          type="button"
          icon={<MdOutlineCancel />}
          className="btnEditModalCancel"
          onClick={handleModalEditComment}
        />
      </div>
    </div>
  );
};

export default ModalEditComment;
