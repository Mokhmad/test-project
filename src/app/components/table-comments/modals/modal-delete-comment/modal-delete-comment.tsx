import Button from "../../../button/button";
import { MdOutlineCancel } from "react-icons/md";
import { useModal } from "../../../../hooks/use-modal-context/use-modal-context";
import { useDispatch } from "react-redux";
import { deleteComment } from "../../../../redux/actions/comments-actions";
import "./modal-delete-comment.scss";

const ModalDeleteComment = () => {
  const dispatch = useDispatch();
  const { toggleModalDelete, commentData } = useModal();
  const handleDeleteComment = () => {
    if (commentData) {
      dispatch(deleteComment(commentData.id));
    }
    toggleModalDelete();
  };
  return (
    <div className="modalDeleteComment">
      <div className="modalDeleteCommentBody">
        <p>точно хотите удалить: {commentData?.name} ?</p>
        <Button
          type="button"
          text="Да"
          className="removeCommentBtn"
          onClick={handleDeleteComment}
        />
        <Button
          type="button"
          text="Нет"
          className="cancelRemoveComment"
          onClick={toggleModalDelete}
        />
        <Button
          type="button"
          icon={<MdOutlineCancel />}
          className="btnCancelDeleteComment"
          onClick={toggleModalDelete}
        />
      </div>
    </div>
  );
};

export default ModalDeleteComment;
