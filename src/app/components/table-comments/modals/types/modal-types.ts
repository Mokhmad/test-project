export type TModalAddCommentData = {
  name: string;
  email: string;
  body: string;
};

export type TErrorCommentData = {
  name: string;
  email: string;
  body: string;
};

export type TCommentData = {
  name: string;
  email: string;
  body: string;
  id: number;
  postId: number;
};

export type TModalAddComment = {
  showModalToggle: () => void;
};

export type TModelEditComment = {
  handleDeleteComment: () => void;
};
