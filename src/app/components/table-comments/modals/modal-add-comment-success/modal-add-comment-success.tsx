import { useEffect } from "react";
import { MdOutlineCancel } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { toggleModalAddCommentSuccess } from "../../../../redux/actions/comments-actions";
import { TInitialState } from "../../../../redux/types/types-comments";
import Button from "../../../button/button";
import "./modal-add-comment-success.scss";

const ModalAddCommentSuccess = () => {
  const dispatch = useDispatch();
  const { newComment } = useSelector((state: TInitialState) => state);

  useEffect(() => {
    setTimeout(() => {
      dispatch(toggleModalAddCommentSuccess());
    }, 5000);
  }, [dispatch]);

  const handleCloseModal = () => {
    dispatch(toggleModalAddCommentSuccess());
  };

  return (
    <div className="createCommentSuceess">
      <div className="createCommentSuccessBody">
        <h4>Комментарий успешно добавлен: </h4>
        <p>{newComment}</p>
        <Button
          type="button"
          icon={<MdOutlineCancel />}
          className="btnCancelCreateCommentSuccess"
          onClick={handleCloseModal}
        />
      </div>
    </div>
  );
};

export default ModalAddCommentSuccess;
