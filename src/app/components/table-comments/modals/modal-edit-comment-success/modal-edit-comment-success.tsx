import { useEffect } from "react";
import { MdOutlineCancel } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { toggleModalEditSuccess } from "../../../../redux/actions/comments-actions";
import { TInitialState } from "../../../../redux/types/types-comments";
import Button from "../../../button/button";

const ModalEditCommentSuccess = () => {
  const dispatch = useDispatch();
  const { editComment } = useSelector((state: TInitialState) => state);

  useEffect(() => {
    setTimeout(() => {
      dispatch(toggleModalEditSuccess());
    }, 5000);
  }, [dispatch]);

  const handleCloseModal = () => {
    dispatch(toggleModalEditSuccess());
  };

  return (
    <div className="createCommentSuceess">
      <div className="createCommentSuccessBody">
        <h4>Комментарий успешно изменен: </h4>
        <p>{editComment}</p>
        <Button
          type="button"
          icon={<MdOutlineCancel />}
          className="btnCancelCreateCommentSuccess"
          onClick={handleCloseModal}
        />
      </div>
    </div>
  );
};

export default ModalEditCommentSuccess;
