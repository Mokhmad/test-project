import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { validator } from "../../../../utils/validator";
import { MdOutlineCancel } from "react-icons/md";
import Button from "../../../button/button";
import TextField from "../../../text-field/text-field";
import "./modal-add-comment.scss";
import {
  addComment,
  toggleModalAddComment,
} from "../../../../redux/actions/comments-actions";
import { TModalAddCommentData, TErrorCommentData } from "../types/modal-types";

const ModalAddComment = () => {
  const dispatch = useDispatch();
  const [data, setData] = useState<TModalAddCommentData>({
    name: "",
    email: "",
    body: "",
  });
  const [errors, setErrors] = useState<TErrorCommentData>({
    name: "",
    email: "",
    body: "",
  });

  const handleToggleModalAddComment = () => {
    dispatch(toggleModalAddComment());
  };

  const handleChange = (e: React.SyntheticEvent): void => {
    const target = e.target as HTMLInputElement;
    setData((prevState) => ({
      ...prevState,
      [target.name]: target.value,
    }));
  };

  useEffect(() => {
    validate();
  }, [data]);

  const validatorConfig = {
    name: {
      isRequired: {
        message: "Имя обезательно для заполнения",
      },
    },
    email: {
      isRequired: {
        message: "Электронная почта обязательна для заполнения",
      },
      isEmail: {
        message: "Email введен некорректно",
      },
    },
    body: {
      isRequired: {
        message: "Текст комментария обязательна для заполнения",
      },
    },
  };

  const validate = () => {
    const errors = validator(data, validatorConfig);
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const isValid = Object.keys(errors).length === 0;

  const handleSubmit = () => {
    dispatch(addComment(data));
  };

  return (
    <div className="modalAddComment">
      <div className="modalBodyAddComments">
        <h2>Add Comment</h2>
        <TextField
          name="name"
          type="text"
          placeholder="Введите имя"
          value={data.name}
          onChange={handleChange}
          error={errors.name}
          className="addCommentInput"
          errorClassName="modalInputName"
        />
        <TextField
          name="email"
          type="email"
          placeholder="Введите почту"
          value={data.email}
          onChange={handleChange}
          error={errors.email}
          className="addCommentInput"
          errorClassName="modalInputEmail"
        />
        <TextField
          name="body"
          type="text"
          placeholder="Введите текст"
          value={data.body}
          onChange={handleChange}
          error={errors.body}
          className="addCommentInput"
          errorClassName="modalInputBody"
        />
        <div>
          <Button
            disabled={!isValid}
            type="button"
            text="Создать"
            className="addCommentBtnsModal"
            onClick={handleSubmit}
          />
          <Button
            type="button"
            text="отмена"
            className="resetCommnentBtnModal"
            onClick={handleToggleModalAddComment}
          />
        </div>
        <Button
          type="button"
          icon={<MdOutlineCancel />}
          className="btnModalCancel"
          onClick={handleToggleModalAddComment}
        />
      </div>
    </div>
  );
};

export default ModalAddComment;
