import { useDispatch } from "react-redux";
import { toggleModalAddComment } from "../../../redux/actions/comments-actions";
import Button from "../../button/button";
import TextField from "../../text-field/text-field";
import { TCommnetHeader } from "../types/comment-type";
import "./table-comments-header.scss";

const TableCommentsHeader = ({
  filterChange,
  searchComment,
}: TCommnetHeader) => {
  const dispatch = useDispatch();
  const handleOpenModalAddComment = () => {
    dispatch(toggleModalAddComment());
  };

  return (
    <div className="tableCommentsHeader">
      <div className="subHeader">
        <div className="searchComments">
          <TextField
            className="searchCommentsInput"
            placeholder="Search commets"
            type="text"
            onChange={filterChange}
            value={searchComment}
          />
        </div>
        <div>
          <Button
            className="addCommentBtn"
            type="button"
            text="Создать"
            onClick={handleOpenModalAddComment}
          />
        </div>
      </div>
    </div>
  );
};

export default TableCommentsHeader;
