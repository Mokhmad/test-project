import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Commentary from "./comment/commentary";
import ModalAddComment from "./modals/modal-add-comment/modal-add-comment";
import TableCommentsHeader from "./table-commnets-header/table-comments-header";
import { TComment, TInitialState } from "../../redux/types/types-comments";
import { loadComments } from "../../redux/actions/comments-actions";
import ModalAddCommentSuccess from "./modals/modal-add-comment-success/modal-add-comment-success";
import TableCommentsSubHeader from "./table-comments-sub-header/table-comments-sub-header";
import ModalEditCommentSuccess from "./modals/modal-edit-comment-success/modal-edit-comment-success";
import ModalEditComment from "./modals/modal-edit-comment/modal-edit-comment";
import ModalEdit from "./modals/modal-edit/modal-edit";
import { useModal } from "../../hooks/use-modal-context/use-modal-context";
import ModalDeleteComment from "./modals/modal-delete-comment/modal-delete-comment";
import "./table-comments.scss";

const TableComments = () => {
  const dispatch = useDispatch();
  const { showModalEditComment } = useSelector((state: TInitialState) => state);
  const {
    comments,
    showModalAddComment,
    showModalAddCommentSuccess,
    showModalEditCommentSuccess,
  } = useSelector((state: TInitialState) => state);

  const [searchComment, setSearchComment] = useState<string>("");
  const handleCommentsSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchComment(e.currentTarget.value as string);
  };

  const filteredComments = comments.filter((comment) =>
    comment.name.toLocaleLowerCase().includes(searchComment.toLocaleLowerCase())
  );

  useEffect(() => {
    dispatch(loadComments());
  }, [dispatch]);

  const { showModalEdit, showModalDelete } = useModal();

  return (
    <div className="tableComments">
      <TableCommentsHeader
        filterChange={handleCommentsSearch}
        searchComment={searchComment}
      />
      <TableCommentsSubHeader />
      <div className="commentaries">
        {filteredComments.map((commentary: TComment) => {
          return <Commentary key={commentary.id} commentary={commentary} />;
        })}
      </div>
      {showModalDelete && <ModalDeleteComment />}
      {showModalAddComment && <ModalAddComment />}
      {showModalAddCommentSuccess && <ModalAddCommentSuccess />}
      {showModalEdit && <ModalEdit />}
      {showModalEditComment && <ModalEditComment />}
      {showModalEditCommentSuccess && <ModalEditCommentSuccess />}
    </div>
  );
};

export default TableComments;
