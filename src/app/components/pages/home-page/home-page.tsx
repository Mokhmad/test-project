import TabLinks from "../../tabs/tab-links/tab-links";
import TabPages from "../../tabs/tab-pages/tab-pages";
import "../home-page/home-page.scss";

const HomePage = () => {
  return (
    <div className="homePage">
      <TabLinks />
      <TabPages />
    </div>
  );
};

export default HomePage;
