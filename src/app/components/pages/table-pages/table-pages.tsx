import { ContextModal } from "../../../hooks/use-modal-context/use-modal-context";
import TableComments from "../../table-comments/table-comments";

const TablePages = () => {
  return (
    <div>
      <ContextModal>
        <TableComments />
      </ContextModal>
    </div>
  );
};

export default TablePages;
