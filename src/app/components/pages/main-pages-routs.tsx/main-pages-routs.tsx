import { lazy, Suspense } from "react";
import { Route, Switch } from "react-router";
import { Routes } from "../../../utils/routes";
import Breadcrumbs from "../../breadcrumbs/breadcrumbs";

const AboutUs = lazy(() => import("../about-as/about-us"));
const HomePage = lazy(() => import("../home-page/home-page"));
const Contacts = lazy(() => import("../contacts/contacts"));
const Table = lazy(() => import("../table-pages/table-pages"));

const MainPagesRouts = () => {
  return (
    <div className="contentContainer">
      <Breadcrumbs />
      <Suspense fallback={<h1>Loading...</h1>}>
        <Switch>
          <Route path={Routes.aboutUs} component={AboutUs} />
          <Route path={Routes.contacts} component={Contacts} />
          <Route path={Routes.homePage} component={HomePage} />
          <Route path={Routes.tablePage} component={Table} />
        </Switch>
      </Suspense>
    </div>
  );
};

export default MainPagesRouts;
