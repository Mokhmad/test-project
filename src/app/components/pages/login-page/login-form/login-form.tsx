import React, { useEffect, useState } from "react";
import { useUserIdentify } from "../../../../hooks/use-user-identify/use-user-Identify";
import { validator } from "../../../../utils/validator";
import Button from "../../../button/button";
import TextField from "../../../text-field/text-field";
import { Data } from "./login-form-type";
import "./login-form.scss";

const LoginForm = () => {
  const [data, setData] = useState<Data>({ email: "", password: "" });
  const [errors, setErrors] = useState<Data>({ email: "", password: "" });

  const handleChange = (e: React.SyntheticEvent): void => {
    const target = e.target as HTMLInputElement;
    setData((prevState) => ({
      ...prevState,
      [target.name]: target.value,
    }));
  };

  const validatorConfig = {
    email: {
      isRequired: {
        message: "Электронная почта обязательна для заполнения",
      },
      isEmail: {
        message: "Email введен некорректно",
      },
    },
    password: {
      isRequired: {
        message: "Пароль обязателен для заполнения",
      },
      isCapitalSymbol: {
        message: "Пароль должен содержать хотя-бы одну заглавную букву",
      },
      isContainDigit: {
        message: "Пароль должен содержать минимум 4 цифры",
      },
      specialCharacters: {
        message: "Пароль должен содержать минимум 2 спец символа",
      },
      min: {
        message: "Пароль должен состоять минимум из 12 символов",
        value: 12,
      },
    },
  };

  useEffect(() => {
    validate();
  }, [data]);

  const clearInputForm = () => {
    setData({ email: "", password: "" });
  };

  const validate = () => {
    const errors = validator(data, validatorConfig);
    setErrors(errors);
    return Object.keys(errors).length === 0;
  };

  const isValid = Object.keys(errors).length === 0;

  const { handleSubmit, error } = useUserIdentify(data);

  return (
    <div className="loginForm">
      <div className="formError">{error}</div>
      <TextField
        label="Email"
        name="email"
        type="email"
        placeholder="Введите почту"
        value={data.email}
        onChange={handleChange}
        error={errors.email}
        className="input"
      />
      <TextField
        label="Password"
        name="password"
        type="password"
        placeholder="Введите пароль"
        value={data.password}
        onChange={handleChange}
        error={errors.password}
        className="input"
      />
      <div className="buttonsForm">
        <Button
          disabled={!isValid}
          text="Отправить"
          type="button"
          className="btnSubmit"
          onClick={handleSubmit}
        />
        <Button
          disabled={!isValid}
          text="Очистить"
          type="reset"
          className="btnReset"
          onClick={clearInputForm}
        />
      </div>
    </div>
  );
};

export default LoginForm;
