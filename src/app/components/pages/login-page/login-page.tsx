import LoginForm from "./login-form/login-form";
import { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Routes } from "../../../utils/routes";
import "./login-page.scss";

const LoginPage = () => {
  const history = useHistory();

  useEffect(
    () => () => {
      if (!localStorage.getItem("user-authorized")) {
        history.push(Routes.loginPage);
      }
    },
    [history]
  );

  return (
    <div className="loginPage">
      <h1>Авторизация</h1>
      <LoginForm />
    </div>
  );
};

export default LoginPage;
