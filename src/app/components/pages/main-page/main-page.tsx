import { useState } from "react";
import Sidebar from "../../sidebar/sidebar";
import Header from "../../header/header";
import Footer from "../../footer/footer";
import MainPagesRouts from "../main-pages-routs.tsx/main-pages-routs";
import "../main-page/main-page.scss";
import "../../sidebar/sidebar.scss";

const MainPage = () => {
  const [linksName, setLinksName] = useState<string>("Header");
  return (
    <div className="mainPage">
      <Header linksName={linksName} />
      <div className="sidebarWithContent">
        <Sidebar setLinksName={setLinksName} />
        <MainPagesRouts />
      </div>
      <Footer />
    </div>
  );
};

export default MainPage;
