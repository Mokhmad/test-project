import { TButton } from "./types/button-types";
import "../button/button.scss";

const Button = ({
  text,
  type,
  disabled,
  onClick,
  className,
  icon,
}: TButton) => {
  return (
    <button
      type={type}
      disabled={disabled}
      onClick={onClick}
      className={className}
    >
      {icon ? icon : text}
    </button>
  );
};

export default Button;
