export type TButton = {
  type: "button" | "submit" | "reset";
  onClick?: () => void;
  text?: string;
  className?: string;
  disabled?: boolean | undefined;
  icon?: any;
};
