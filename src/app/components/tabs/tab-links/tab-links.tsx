import { NavLink } from "react-router-dom";
import { useLocalStorage } from "../../../hooks/use-local-storage/use-local-storage";
import { Routes } from "../../../utils/routes";
import "./tab-links.scss";

const TabLinks = () => {
  const { checkUserIsAdmin } = useLocalStorage();
  const isAdmin = checkUserIsAdmin("user-authorized");

  return (
    <div className="tabLinks">
      <NavLink to={Routes.tab1} className="tab" activeClassName="activeTab">
        Tab1
      </NavLink>
      <NavLink to={Routes.tab2} className="tab" activeClassName="activeTab">
        Tab2
      </NavLink>
      <NavLink to={Routes.tab3} className="tab" activeClassName="activeTab">
        Tab3
      </NavLink>
      {isAdmin && (
        <NavLink
          to={Routes.tabAdmin}
          className="tab"
          activeClassName="activeTab"
        >
          Tab Admin
        </NavLink>
      )}
    </div>
  );
};

export default TabLinks;
