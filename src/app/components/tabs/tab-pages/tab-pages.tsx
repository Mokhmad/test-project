import { Route, Switch } from "react-router-dom";
import { Routes } from "../../../utils/routes";
import TabAdmin from "./tab-admin";
import TabOne from "./tab-one";
import TabThree from "./tab-three";
import TabTwo from "./tab-two";

const TabPages = () => {
  return (
    <div>
      <Switch>
        <Route path={Routes.tab1} component={TabOne} />
        <Route path={Routes.tab2} component={TabTwo} />
        <Route path={Routes.tab3} component={TabThree} />
        <Route path={Routes.tabAdmin} component={TabAdmin} />
      </Switch>
    </div>
  );
};

export default TabPages;
