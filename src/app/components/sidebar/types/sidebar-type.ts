import { Dispatch, SetStateAction } from "react";

export type TSidebar = {
  setLinksName: Dispatch<SetStateAction<string>>;
};
