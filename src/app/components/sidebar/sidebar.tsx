import { FaBars } from "react-icons/fa";
import { MdDashboard } from "react-icons/md";
import { BiLogOut } from "react-icons/bi";
import { FcContacts, FcAbout } from "react-icons/fc";
import { GrUserAdmin } from "react-icons/gr";
import { NavLink } from "react-router-dom";
import { useState } from "react";
import { Routes } from "../../utils/routes";
import Button from "../button/button";
import { useLocalStorage } from "../../hooks/use-local-storage/use-local-storage";
import { TSidebar } from "./types/sidebar-type";
import { AiOutlineTable } from "react-icons/ai";

const Sidebar = ({ setLinksName }: TSidebar) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleClassActive = () => setIsOpen(!isOpen);
  const { removeUserAuthorized, checkUserIsAdmin } = useLocalStorage();
  const isAdmin = checkUserIsAdmin("user-authorized");
  const getLinksName = (e: React.MouseEvent<HTMLElement>) => {
    setLinksName(e.currentTarget.textContent as string);
  };

  return (
    <div className={`sidebar ${isOpen ? "active" : ""}`}>
      <Button
        icon={<FaBars />}
        className="btn"
        onClick={toggleClassActive}
        type="button"
      />
      <ul>
        <li>
          <NavLink
            to={Routes.homePage + "/tab1"}
            activeClassName="activeLink"
            onClick={getLinksName}
          >
            <span>
              <MdDashboard />
            </span>
            <span className="linksName">Home</span>
          </NavLink>
        </li>
        <li>
          <NavLink
            to={Routes.tablePage}
            activeClassName="activeLink"
            onClick={getLinksName}
          >
            <span>
              <AiOutlineTable />
            </span>
            <span className="linksName" onClick={getLinksName}>
              Table
            </span>
          </NavLink>
        </li>
        <li>
          <NavLink
            to={Routes.aboutUs}
            activeClassName="activeLink"
            onClick={getLinksName}
          >
            <span>
              <FcAbout />
            </span>
            <span className="linksName">About</span>
          </NavLink>
        </li>
        <li>
          <NavLink
            to={Routes.contacts}
            activeClassName="activeLink"
            onClick={getLinksName}
          >
            <span>
              <FcContacts />
            </span>
            <span className="linksName" onClick={getLinksName}>
              Contacts
            </span>
          </NavLink>
        </li>
        {isAdmin && (
          <li>
            <NavLink
              to={Routes.admin}
              activeClassName="activeLink"
              onClick={getLinksName}
            >
              <span>
                <GrUserAdmin />
              </span>
              <span className="linksName" onClick={getLinksName}>
                Admin
              </span>
            </NavLink>
          </li>
        )}
      </ul>
      <Button
        icon={<BiLogOut />}
        className="logOut"
        onClick={removeUserAuthorized}
        type="button"
      />
    </div>
  );
};

export default Sidebar;
