export const initialState = {
  comments: [],
  loading: false,
  showModalAddComment: false,
  showModalAddCommentSuccess: false,
  showModalEditCommentSuccess: false,
  showModalEditComment: false,
  newComment: "",
  editComment: "",
};
