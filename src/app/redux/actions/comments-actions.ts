import { Dispatch } from "redux";
import {
  TCommentData,
  TModalAddCommentData,
} from "../../components/table-comments/modals/types/modal-types";

import { CommentsActionTypes } from "../types/types-comments";

export const loadComments = () => (dispatch: Dispatch) => {
  dispatch({ type: CommentsActionTypes.LOAD_COMMENTS_START });
  fetch("http://localhost:8080/comments")
    .then((response) => response.json())
    .then((data) => {
      dispatch({
        type: CommentsActionTypes.LOAD_COMMENTS_SUCCESS,
        payload: data,
      });
    });
};

export const deleteComment = (id: number) => (dispatch: Dispatch) => {
  fetch(`http://localhost:8080/comments/${id}`, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then(() => {
      dispatch({
        type: CommentsActionTypes.DELETE_COMMENT_SUCCESS,
        payload: id,
      });
    });
};

export const addComment =
  (data: TModalAddCommentData) => (dispatch: Dispatch) => {
    dispatch({ type: CommentsActionTypes.ADD_COMMENT_START });
    fetch("http://localhost:8080/comments", {
      method: "POST",
      body: JSON.stringify({
        name: data.name,
        body: data.body,
        email: data.email,
        postId: Date.now(),
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
    })
      .then((response) => response.json())
      .then((data) => {
        dispatch({
          type: CommentsActionTypes.ADD_COMMENT_SUCCESS,
          payload: data,
        });
      });
  };

export const editComment = (data: TCommentData) => (dispatch: Dispatch) => {
  fetch(`http://localhost:8080/comments/${data.id}`, {
    method: "PUT",
    body: JSON.stringify({
      id: data.id,
      name: data.name,
      email: data.email,
      body: data.body,
      postId: data.postId,
    }),
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  })
    .then((response) => response.json())
    .then((data) => {
      dispatch({
        type: CommentsActionTypes.EDIT_COMMENT_SUCCESS,
        payload: data,
      });
    });
};

export const sortNameComments = (value: boolean) => {
  return { type: CommentsActionTypes.SORT_COMMENTS_NAME, payload: value };
};

export const sortBodyComments = (value: boolean) => {
  return { type: CommentsActionTypes.SORT_COMMENTS_BODY, payload: value };
};

export const sortEmailComments = (value: boolean) => {
  return { type: CommentsActionTypes.SORT_COMMENTS_EMAIL, payload: value };
};

export const toggleModalAddComment = () => {
  return { type: CommentsActionTypes.TOGGLE_MODAL_ADD_COMMENT };
};

export const toggleModalAddCommentSuccess = () => {
  return { type: CommentsActionTypes.CLOSE_MODAL_SUCCESS };
};

export const toggleModalEditComment = () => {
  return { type: CommentsActionTypes.SHOW_MODAL_EDIT_COMMENT };
};

export const toggleModalEditSuccess = () => {
  return { type: CommentsActionTypes.CLOSE_MODAL_EDIT_SUCCESS };
};
