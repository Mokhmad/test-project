import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { commentsReducer } from "./reducers/comments-reducer";
import { createLogger } from "redux-logger";

const logger = createLogger({
  diff: true,
  collapsed: true,
});

export const store = createStore(
  commentsReducer,
  applyMiddleware(thunk, logger)
);
