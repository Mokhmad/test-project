export enum CommentsActionTypes {
  LOAD_COMMENTS_START = "LOAD_COMMENTS_START",
  LOAD_COMMENTS_SUCCESS = "LOAD_COMMENTS_SUCCESS",
  ADD_COMMENT_START = "ADD_COMMENT_START",
  ADD_COMMENT_SUCCESS = "ADD_COMMENT_SUCCESS",
  DELETE_COMMENT_START = "DELETE_COMMENT_START",
  DELETE_COMMENT_SUCCESS = "DELETE_COMMENT_SUCCESS",
  EDIT_COMMENT_SUCCESS = "EDIT_COMMENT_SUCCESS",
  EDIT_COMMENT_START = "EDIT_COMMENT_START",
  SORT_COMMENTS_NAME = "SORT_COMMENTS_NAME",
  SORT_COMMENTS_BODY = "SORT_COMMENTS_BODY",
  SORT_COMMENTS_EMAIL = "SORT_COMMENTS_EMAIL",
  TOGGLE_MODAL_ADD_COMMENT = "TOGGLE_MODAL_ADD_COMMENT",
  CLOSE_MODAL_SUCCESS = "CLOSE_MODAL_SUCCESS",
  CLOSE_MODAL_EDIT_SUCCESS = "CLOSE_MODAL_EDIT_SUCCESS",
  SHOW_MODAL_EDIT_COMMENT = "SHOW_MODAL_EDIT_COMMENT",
}

export type TInitialState = {
  comments: TComment[];
  loading: boolean;
  showModalAddComment: boolean;
  showModalAddCommentSuccess: boolean;
  newComment: string;
  editComment: string;
  showModalEditComment: boolean;
  showModalEditCommentSuccess: boolean;
};

export type TComment = {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
};

type EditCommentSuccess = {
  type: CommentsActionTypes.EDIT_COMMENT_SUCCESS;
  payload: TComment;
};

type SortCommentsName = {
  type: CommentsActionTypes.SORT_COMMENTS_NAME;
  payload: boolean;
};

type SortCommentsBody = {
  type: CommentsActionTypes.SORT_COMMENTS_BODY;
  payload: boolean;
};

type SortCommentsEmail = {
  type: CommentsActionTypes.SORT_COMMENTS_EMAIL;
  payload: boolean;
};

type LoadCommentsStart = {
  type: CommentsActionTypes.LOAD_COMMENTS_START;
};

type LoadCommentsSuccess = {
  type: CommentsActionTypes.LOAD_COMMENTS_SUCCESS;
  payload: TComment[];
};

type DeleteCommentStart = {
  type: CommentsActionTypes.DELETE_COMMENT_START;
};

type DeleteCommentSuccess = {
  type: CommentsActionTypes.DELETE_COMMENT_SUCCESS;
  payload: number;
};

type AddCommentStart = {
  type: CommentsActionTypes.ADD_COMMENT_START;
};

type AddCommentSuccess = {
  type: CommentsActionTypes.ADD_COMMENT_SUCCESS;
  payload: TComment;
};

type ToggleModalAddComment = {
  type: CommentsActionTypes.TOGGLE_MODAL_ADD_COMMENT;
};

type CloseModalSuccess = {
  type: CommentsActionTypes.CLOSE_MODAL_SUCCESS;
};

type CloseModalEditSuccess = {
  type: CommentsActionTypes.CLOSE_MODAL_EDIT_SUCCESS;
};

type ShowModalEditComment = {
  type: CommentsActionTypes.SHOW_MODAL_EDIT_COMMENT;
};

export type CommentsAction =
  | LoadCommentsStart
  | LoadCommentsSuccess
  | DeleteCommentSuccess
  | DeleteCommentStart
  | AddCommentStart
  | AddCommentSuccess
  | SortCommentsName
  | SortCommentsBody
  | SortCommentsEmail
  | EditCommentSuccess
  | ToggleModalAddComment
  | CloseModalSuccess
  | CloseModalEditSuccess
  | ShowModalEditComment;
