import { initialState } from "../initial-state/initial-state-comments";
import {
  CommentsAction,
  CommentsActionTypes,
  TComment,
  TInitialState,
} from "../types/types-comments";

export const commentsReducer = (
  state: TInitialState = initialState,
  action: CommentsAction
): TInitialState => {
  switch (action.type) {
    case CommentsActionTypes.LOAD_COMMENTS_START: {
      return { ...state, loading: true };
    }
    case CommentsActionTypes.LOAD_COMMENTS_SUCCESS: {
      return { ...state, loading: false, comments: action.payload };
    }

    case CommentsActionTypes.DELETE_COMMENT_SUCCESS: {
      return {
        ...state,
        comments: state.comments.filter(
          (comment) => comment.id !== action.payload
        ),
      };
    }

    case CommentsActionTypes.ADD_COMMENT_SUCCESS: {
      return {
        ...state,
        showModalAddComment: !state.showModalAddComment,
        showModalAddCommentSuccess: true,
        newComment: action.payload.name,
        comments: [action.payload, ...state.comments],
      };
    }

    case CommentsActionTypes.EDIT_COMMENT_SUCCESS: {
      const data = action.payload;
      return {
        ...state,
        editComment: data.name,
        showModalEditComment: false,
        showModalEditCommentSuccess: true,
        comments: state.comments.map((comment) => {
          if (comment.id === data.id) {
            return data;
          }
          return comment;
        }),
      };
    }

    case CommentsActionTypes.SORT_COMMENTS_NAME: {
      return {
        ...state,
        comments: state.comments.sort((a: TComment, b: TComment): number =>
          action.payload ? (a > b ? 1 : -1) : a < b ? 1 : -1
        ),
      };
    }

    case CommentsActionTypes.SORT_COMMENTS_BODY: {
      return {
        ...state,
        comments: state.comments.sort((a: TComment, b: TComment): number =>
          action.payload ? (a > b ? 1 : -1) : a < b ? 1 : -1
        ),
      };
    }

    case CommentsActionTypes.SORT_COMMENTS_EMAIL: {
      return {
        ...state,
        comments: state.comments.sort((a: TComment, b: TComment): number =>
          action.payload ? (a > b ? 1 : -1) : a < b ? 1 : -1
        ),
      };
    }

    case CommentsActionTypes.TOGGLE_MODAL_ADD_COMMENT: {
      return {
        ...state,
        showModalAddComment: !state.showModalAddComment,
      };
    }

    case CommentsActionTypes.CLOSE_MODAL_SUCCESS: {
      return {
        ...state,
        showModalAddCommentSuccess: false,
      };
    }

    case CommentsActionTypes.CLOSE_MODAL_EDIT_SUCCESS: {
      return {
        ...state,
        showModalEditCommentSuccess: false,
      };
    }

    case CommentsActionTypes.SHOW_MODAL_EDIT_COMMENT: {
      return {
        ...state,
        showModalEditComment: !state.showModalEditComment,
      };
    }

    default: {
      return state;
    }
  }
};
