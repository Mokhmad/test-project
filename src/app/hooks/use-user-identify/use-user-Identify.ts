import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Routes } from "../../utils/routes";
import { usersData } from "../../utils/user-data";
import { useLocalStorage } from "../use-local-storage/use-local-storage";
import { TUser, TUserIdentify } from "./types/identify-type";

export const useUserIdentify = (data: TUser): TUserIdentify => {
  const { email, password } = data;
  const [error, setError] = useState<string>("");
  const history = useHistory();

  const { setUserAuthorized } = useLocalStorage();

  const isUserAuthorized = usersData.some((user) => {
    return user.email === email && user.password === password;
  });

  const isAdmin = usersData.some(
    (user) => user.email === email && user.isAdmin
  );

  const handleSubmit = (): void => {
    if (isUserAuthorized) {
      setUserAuthorized("user-authorized", { email, isAdmin });
      history.push(Routes.mainPage);
    } else {
      setError("Неправильный логин или пароль");
    }
  };

  return {
    handleSubmit,
    error,
  };
};
