export type TUserIdentify = {
  handleSubmit: () => void;
  error: string;
};

export type TUser = {
  email?: string;
  password?: string;
  isAdmin?: boolean;
};
