import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { Routes } from "../../utils/routes";
import { TUserIsAuthorized } from "./types/user-authorization-type";
import { useLocalStorage } from "./use-local-storage";

export const useCheckingUserAuthorization = (): TUserIsAuthorized => {
  const [isAuthorized, setIsAuthorized] = useState<boolean>(false);

  const { stateLocalStorage, localUserIsAuthorized } = useLocalStorage();
  const history = useHistory();

  useEffect(() => {
    localUserIsAuthorized("user-authorized");
  }, [localUserIsAuthorized]);

  useEffect(() => {
    if (stateLocalStorage) {
      history.push(Routes.mainPage);
      setIsAuthorized(true);
    } else {
      history.push(Routes.loginPage);
      setIsAuthorized(false);
    }
  }, [history, stateLocalStorage]);

  useEffect(() => {
    return history.listen(() => {
      if (history.action === "POP") {
        if (!isAuthorized) {
          history.push(Routes.mainPage);
        }
      }
    });
  }, [history, isAuthorized, history.location]);

  return {
    isAuthorized,
  };
};
