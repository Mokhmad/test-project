import { useState } from "react";
import { useHistory } from "react-router-dom";
import { Routes } from "../../utils/routes";
import { TUser } from "../use-user-identify/types/identify-type";

export const useLocalStorage = () => {
  const history = useHistory();
  const [stateLocalStorage, setStateLocalStorage] = useState<string | any>(
    null
  );

  const userIsAuthorized = (key: string): string | null => {
    return localStorage.getItem(key);
  };

  const localUserIsAuthorized = (key: string): void => {
    if (userIsAuthorized(key)) {
      setStateLocalStorage(localStorage.getItem(key));
    }
  };

  const setUserAuthorized = (key: string, email: TUser) => {
    localStorage.setItem(key, JSON.stringify(email));
  };

  const checkUserIsAdmin = (key: string): boolean => {
    const getUserIsAdmin = localStorage.getItem(key);
    const isAdmin = !!getUserIsAdmin && JSON.parse(getUserIsAdmin);
    return isAdmin.isAdmin;
  };

  const removeUserAuthorized = () => {
    localStorage.removeItem("user-authorized");
    history.push(Routes.loginPage);
  };

  return {
    stateLocalStorage,
    localUserIsAuthorized,
    setUserAuthorized,
    removeUserAuthorized,
    checkUserIsAdmin,
  };
};
