export type TUserIsAuthorized = {
  isAuthorized: boolean;
};
