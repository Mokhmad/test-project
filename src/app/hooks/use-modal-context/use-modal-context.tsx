import { useContext, useState, createContext, ReactNode } from "react";

export type TCommentData = {
  name: string;
  email: string;
  body: string;
  id: number;
  postId: number;
};

type TContextModal = {
  children: ReactNode;
};

type TModalContext = {
  showModalEdit: boolean;
  showModalDelete: boolean;
  commentData: TCommentData;
  getCommentData: (data: TCommentData) => void;
  toggleModalEdit: () => void;
  toggleModalDelete: () => void;
};

const initialState = {
  showModalEdit: false,
  showModalDelete: false,
  commentData: {
    name: "",
    email: "",
    body: "",
    id: 0,
    postId: 0,
  },
  getCommentData: () => {},
  toggleModalEdit: () => {},
  toggleModalDelete: () => {},
};

const ModalContext = createContext<TModalContext>(initialState);

export const useModal = () => {
  return useContext(ModalContext);
};

export const ContextModal = ({ children }: TContextModal) => {
  const [showModalEdit, setShowModalEdit] = useState<boolean>(false);
  const [showModalDelete, setShowModalDelete] = useState<boolean>(false);
  const toggleModalDelete = () => setShowModalDelete(!showModalDelete);
  const toggleModalEdit = () => setShowModalEdit(!showModalEdit);

  const [commentData, setCommentData] = useState<TCommentData>({
    name: "",
    email: "",
    body: "",
    id: 0,
    postId: 0,
  });
  const getCommentData = (data: TCommentData) => {
    setCommentData(data);
  };

  return (
    <ModalContext.Provider
      value={{
        showModalEdit,
        showModalDelete,
        commentData,
        getCommentData,
        toggleModalEdit,
        toggleModalDelete,
      }}
    >
      {children}
    </ModalContext.Provider>
  );
};
