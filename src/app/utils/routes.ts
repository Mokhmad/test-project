export enum Routes {
  mainPage = "/Main",
  homePage = "/Main/Home",
  contacts = "/Main/Contacts",
  aboutUs = "/Main/About Us",
  admin = "/Main/Admin",
  loginPage = "/login-page",
  tab1 = "/Main/Home/tab1",
  tab2 = "/Main/Home/tab2",
  tab3 = "/Main/Home/tab3",
  tabAdmin = "/Main/Home/Admin",
  tablePage = "/Main/Table",
}
