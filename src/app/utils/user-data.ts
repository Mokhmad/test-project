export const usersData = [
  {
    id: 1,
    email: "admin@mail.ru",
    isAdmin: true,
    password: "Admin12345!.",
  },
  {
    id: 2,
    email: "user@mail.ru",
    isAdmin: false,
    password: "Users12345!.",
  },
];
