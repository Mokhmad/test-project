import { Routes } from "./routes";

export const headerTitles = (title: string): string => {
  switch (title) {
    case Routes.homePage: {
      return "Home";
    }
    case Routes.aboutUs: {
      return "About AS";
    }
    case Routes.contacts: {
      return "Contacts";
    }
    default: {
      return "Header";
    }
  }
};
