export function validator(data: any, config: any) {
  const errors: any = {};
  function validate(validateMethod: any, data: any, config: any) {
    let statusValidate;
    switch (validateMethod) {
      case "isRequired": {
        statusValidate = data.trim() === "";
        break;
      }
      case "isEmail": {
        const emailRedExp = /^\S+@\S+\.\S+$/g;
        statusValidate = !emailRedExp.test(data);
        break;
      }
      case "isCapitalSymbol": {
        const capitalRedExp = /[A-Z]+/g;
        statusValidate = !capitalRedExp.test(data);
        break;
      }
      case "isContainDigit": {
        const digitRedExp = /\d{4}/g;
        statusValidate = !digitRedExp.test(data);
        break;
      }
      case "min": {
        statusValidate = data.length < config.value;
        break;
      }
      case "specialCharacters": {
        const specialCharacters = /[!@#$%^&*()\-=_+~[\]{}'"\\|../<>?]{2}/g;
        statusValidate = !specialCharacters.test(data);
        break;
      }
      default:
        break;
    }
    if (statusValidate) return config.message;
  }

  for (const fieldName in data) {
    for (const validateMethod in config[fieldName]) {
      const error = validate(
        validateMethod,
        data[fieldName],
        config[fieldName][validateMethod]
      );

      if (error && !errors[fieldName]) {
        errors[fieldName] = error;
      }
    }
  }

  return errors;
}
